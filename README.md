# marineresourcemodelling.gitlab.io

Group website


## Adding or Updating an R package

If you add/delete/update an R package in the *public/sran/* folder, then make sure to rebuild the package index files:

```
Rscript -e 'tools::write_PACKAGES("public/sran/src/contrib/", type="source")'

Rscript -e 'tools::write_PACKAGES("public/sran/bin/windows/contrib/3.6/", type="win.binary")'
Rscript -e 'tools::write_PACKAGES("public/sran/bin/windows/contrib/4.0/", type="win.binary")'

Rscript -e 'tools::write_PACKAGES("public/sran/bin/macosx/contrib/3.6/", type="mac.binary")'
Rscript -e 'tools::write_PACKAGES("public/sran/bin/macosx/contrib/4.0/", type="mac.binary")'
```

Or from an R CMD session, just...

```
tools::write_PACKAGES("public/sran/src/contrib/", type="source")

tools::write_PACKAGES("public/sran/bin/windows/contrib/3.6/", type="win.binary")
tools::write_PACKAGES("public/sran/bin/windows/contrib/4.0/", type="win.binary")

tools::write_PACKAGES("public/sran/bin/macosx/contrib/3.6/", type="mac.binary")
tools::write_PACKAGES("public/sran/bin/macosx/contrib/4.0/", type="mac.binary")
```


you don't have to immediately delete the old version of the package - the *write_PACKAGES* function just picks up the highest release version and will be the one used for any install.packages() calls.

Older releases can be accessed directly - see the *public/sran/index.html* for details.


